$ = jQuery

$(document).on 'ready', () ->

    $parent = $('#map')

    width = $parent.width()
    height = 350

    color = d3.scale.category20()

    force = d3.layout.force().charge(-120).linkDistance(40).size([width, height])

    svg = d3.select("div#map")
        .append("svg")
        .attr('id', 'svg-map')
        .attr("width", width)
        .attr("height", height)
        .attr('viewBox', "0 0 #{width} 300")
        .attr('preserveAspectRatio', 'xMidYMid')

    $map = $('#svg-map')

    $(window).on 'resize', () ->
        w = $map.parent().width()
        $map.attr 'width', w

    d3.json '/home/systems.json', (error, graph) ->
        force.nodes(graph.nodes).links(graph.links).start()

        link = svg.selectAll('.link')
            .data(graph.links)
            .enter()
            .append('line')
            .attr('class', 'link')
            .style('stroke-width', 1)

        node = svg.selectAll(".node")
            .data(graph.nodes)
            .enter()
            .append("circle")
                .attr("class", "node")
                .attr("r", 5)
                .style("fill", (d) -> 
                    if d.faction == 500002
                        return '#B7410E'
                    else if d.faction == 500003
                        return 'orange'
                    else
                        return 'blue'
                )
            .call(force.drag)

        text = svg.selectAll('.label')
            .data(graph.nodes)
            .enter()
            .append('text')
            .attr('class', 'label')
            .text((d) -> return d.name)

        desc = svg.append('text')
            .attr('class', 'description')
            .attr('x', 10)
            .attr('y', 0)
            .text('Current Faction Warfare Zone')

        min = svg.append('text')
            .attr('class', 'description')
            .attr('x', 10)
            .attr('y', 20)
            .text('Minmatar Republic')
            .style('fill', '#B7410E')

        amarr = svg.append('text')
            .attr('class', 'description')
            .attr('x', 10)
            .attr('y', 40)
            .text('Amarr Empire')
            .style('fill', 'orange')

        node.append('title').text((d) -> return d.name)

        force.on 'tick', () ->
            link.attr 'x1', (d) -> return d.source.x
            link.attr 'y1', (d) -> return d.source.y
            link.attr 'x2', (d) -> return d.target.x
            link.attr 'y2', (d) -> return d.target.y

            node.attr 'cx', (d) -> return d.x
            node.attr 'cy', (d) -> return d.y

            text.attr 'transform', (d) ->
                return "translate(#{d.x + 10}, #{d.y})"

    alliance = d3.select('div#alliance-chart')
        .append('svg')
        .attr('id', 'svg-alliance')
        .attr('width', '100%')
        .attr('height', 300)

    d3.json '/home/alliance.json', (error, graph) ->
        x = d3.scale.linear()
            .domain([0, d3.max(graph.corporations, (d) -> return d.memberCount)])
            .range([0, 200])

        bars = alliance.selectAll('.bar')
            .data(graph.corporations)
            .enter().append('rect')
            .attr('class', 'bar')
            .attr('width', (d) -> return x(d.memberCount))
            .attr('height', 15)
            .attr('x', 20)
            .attr('y', (d, i) -> return i * 20 + 5)

        legend = alliance.selectAll('.legend')
            .data(graph.corporations)
            .enter().append('text')
            .attr('class', 'legend')
            .attr('x', (d) -> return x(d.memberCount) + 25)
            .attr('y', (d, i) -> return i * 20 + 17)
            .text((d) -> return "#{d.corporationName} (#{d.memberCount})")

        logo = alliance.selectAll('.logo')
            .data(graph.corporations)
            .enter().append('image')
            .attr('class', 'logo')
            .attr('xlink:href', (d) -> return "http://image.eveonline.com/Corporation/#{d.corporationID}_30.png")
            .attr('width', 15)
            .attr('height', 15)
            .attr('x', 0)
            .attr('y', (d, i) -> return i * 20 + 5)

        $('span.loading').remove()