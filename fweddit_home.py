# -*- coding: utf-8 -*-
"""
    Fweddit Homepage
    ----------------
    This is the I Whip My Slaves Back and Forth (J4LP) alliance homepage.
    Created by adrien-f (Vadrin Hegirin)


"""

from bottle import route, run, static_file, redirect, jinja2_view as view
from bottle import debug

from sqlalchemy import create_engine
from sqlalchemy.sql import select

import json
from operator import itemgetter
from calendar import monthrange
import datetime, time

import eveapi
from redisevecache import RedisEveAPICacheHandler
import redis

import requests

from settings import DB

engine = create_engine(DB)

@route('/')
@view('templates/index.html')
def index():

    r = redis.StrictRedis()
    kill_stats = r.get('kills_stats')
    if kill_stats is not None:
        kill_stats = json.loads(kill_stats)
        if kill_stats[0] < time.time():
            # Expired
            update_kills()
            kill_stats = r.get('kill_stats')
    else:
        update_kills()
        kill_stats = r.get('kill_stats')
        kill_stats = json.loads(kill_stats)    
    
    return dict(
        total_kills=kill_stats[1]['total_kills'], 
        total_kills_isk=kill_stats[1]['total_kills_isk'], 
        total_losses=kill_stats[1]['total_losses'], 
        total_losses_isk=kill_stats[1]['total_losses_isk'],
        last_kills=kill_stats[1]['last_kills'],
    )

def update_kills():
    now = datetime.datetime.now()
    year = now.year
    month = now.month
    last_day = monthrange(year, month)[1]

    # Kills
    r = requests.get('http://eve-kill.net/epic/involvedAllianceID:7875/startDate:' + str(year) + '-' + "%02d" % (month,) + '-01/endDate:' + str(year) + '-' + str(month) + '-' + str(last_day) + '/mailLimit:500/mask:2097151')
    kills = json.loads(r.text)
    last_kills = kills[:10]
    last_kills = [
        {
            'victimShipName': x['victimShipName'], 
            'url': x['url'],
            'ISK_format': "{:,}".format(x['ISK']),
            'ISK': x['ISK']
        } 
    for x in last_kills if x['FBAllianceName'] == "I Whip My Slaves Back and Forth"]
    all_kills = [x for x in kills if x['FBAllianceName'] == 'I Whip My Slaves Back and Forth']
    total_kills = len(all_kills)
    total_kills_isk = sum(x['ISK'] for x in all_kills)

    # Losses
    r = requests.get('http://eve-kill.net/epic/victimAllianceID:7875/startDate:' + str(year) + '-' + "%02d" % (month,) + '-01/endDate:' + str(year) + '-' + str(month) + '-' + str(last_day) + '/mailLimit:500/mask:2097151')
    losses = json.loads(r.text)
    all_losses = [x for x in losses if x['victimAllianceName'] == 'I Whip My Slaves Back and Forth']
    total_losses = len(all_losses)
    total_losses_isk = sum(x['ISK'] for x in all_losses)

    r = redis.StrictRedis()
    r.set('kills_stats', json.dumps((time.time() + 3600, {
        'total_kills': total_kills,
        'total_kills_isk': "{:,}".format(total_kills_isk),
        'total_losses': total_losses, 
        'total_losses_isk': "{:,}".format(total_losses_isk),
        'last_kills': last_kills,
    })))

def update_warzone():
    # Getting the warzone
    warzone = engine.execute('\
        SELECT \
            ROUND(x) / 10000000000000000 AS x,\
            ROUND(y) / 10000000000000000 AS y,\
            solarSystemID, solarSystemName, security, constellationID\
        FROM mapSolarSystems\
        WHERE solarSystemID IN (\
            SELECT solarSystemID\
            FROM warCombatZoneSystems\
            WHERE combatZoneID IN (3, 6)\
        )\
    ')

    # From the api
    api = eveapi.EVEAPIConnection(cacheHandler=RedisEveAPICacheHandler(debug=True))
    api_map = api.map.FacWarSystems()
    faction_map = {}
    for system in api_map.solarSystems:
        if system['occupyingFactionID'] == 0:
            faction_map[system['solarSystemID']] = system['owningFactionID']
        else:
            faction_map[system['solarSystemID']] = system['occupyingFactionID']

    # Basic json structure
    wz_json = {'nodes': [], 'links': []}

    # List comprehension to populate the nodes
    wz_json['nodes'] = [
        {
            'name': x['solarSystemName'], 
            'group': x['constellationID'],
            'id': x['solarSystemID'],
            'security': "%.1f" % x['security'],
            'faction': faction_map[x['solarSystemID']]
        } 
        for x in warzone
    ]

    # Querying the jumps between the systems
    jumps = engine.execute('\
        SELECT \
            fromSolarSystemID, toSolarSystemID\
        FROM mapSolarSystemJumps\
        WHERE fromSolarSystemID IN (\
            SELECT solarSystemID\
            FROM warCombatZoneSystems\
            WHERE combatZoneID IN (3, 6))\
    ')

    # Populating it
    # The next helps us finds the index of the item in the nodes list
    wz_json['links'] = [
        {
            'source': next((ind for ind, y in enumerate(wz_json['nodes']) if y['id'] == x['fromSolarSystemID']), 0),
            'target': next((ind for ind, y in enumerate(wz_json['nodes']) if y['id'] == x['toSolarSystemID']), -1)
        }
        for x in jumps
    ]

    # Cleaning the links
    wz_json['links'] = [x for x in wz_json['links'] if not (x['source'] == 0 and x['target'] == 0)]
    wz_json['links'] = [x for x in wz_json['links'] if not (x['target'] == -1)]
    
    r = redis.StrictRedis()
    r.set('systems.json', json.dumps(wz_json))


@route('/systems.json')
def systems():
    r = redis.StrictRedis()
    return r.get('systems.json')


@route('/alliance.json')
def alliance():
    alliance = {'memberCount': 0, 'corporations': []}

    # Setting up the API Connection
    api = eveapi.EVEAPIConnection(cacheHandler=RedisEveAPICacheHandler(debug=True))

    # Retrieving the alliance list
    allianceList = api.eve.AllianceList()

    # I whip my slaves back and forth

    for a in allianceList.alliances:
        if a.allianceID == 99002172:
            j4lp = a
            break
    alliance['memberCount'] = j4lp.memberCount

    # Corporations List
    alliance['corporations'] = [
        api.corp.corporationSheet(corporationID=x['corporationID'])
        for x in j4lp.memberCorporations
    ]
    alliance['corporations'] = [
        {
            'corporationID': x.corporationID,
            'corporationName': x.corporationName,
            'memberCount': x.memberCount,
        }
        for x in alliance['corporations']
    ]

    # Ordering list
    alliance['corporations'] = sorted(alliance['corporations'], key=itemgetter('memberCount'), reverse=True)

    return json.dumps(alliance)

@route('/static/<filepath:path>')
def serve_static(filepath):
    return static_file(filepath, root='static')

@route('/favicon.ico')
def serve_favicon():
    return static_file('favicon.ico', 'static')





if __name__ == '__main__':    
    run(host='0.0.0.0', port=8000, debug=True, reloader=True, server='cherrypy')