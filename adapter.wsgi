import sys, os, bottle

sys.path = ['/srv/fweddit_home/'] + sys.path
os.chdir(os.path.dirname(__file__))

import fweddit_home # This loads your application

application = bottle.default_app()